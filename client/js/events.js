"use strict";

Template.form.events({
    'submit .add-new-task': function (event) {
        event.preventDefault();
        
        if (event.currentTarget.children[0].firstElementChild.value === "") {
            alert("you cant have a emptey task");
        } else {
            var taskName = event.currentTarget.children[0].firstElementChild.value;
            Collections.Todo.insert({
                name: taskName,
                createdAt: new Date(),
                complete: false
            });
        }
        event.currentTarget.children[0].firstElementChild.value = "";
        return false;
    }
});


Template.todos.events({
    'click .delete-task': function (event) {
        Collections.Todo.remove({
            _id: this._id
        });
        
    },

    'click .complete-task': function (event) {
        Collections.Todo.update({
            _id: this._id
        }, {
            $set: {
                complete: true
            }
        });
    },
    
    'click .edit-task': function (event) {
        var edit = prompt("What would you liike to change it to.");
        Collections.Todo.update({
            _id: this._id
        }, {
            $set: {
                name: edit
            }
        });
    },
  
    'click .incomplete-task': function (event) {
        Collections.Todo.update({
            _id: this._id
        }, {
            $set: {
                complete: false
            }
        });
    }
});